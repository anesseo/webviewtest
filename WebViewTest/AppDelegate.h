//
//  AppDelegate.h
//  WebViewTest
//
//  Created by wpeople on 13. 2. 19..
//  Copyright (c) 2013년 hyeyoung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
