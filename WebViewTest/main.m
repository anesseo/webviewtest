//
//  main.m
//  WebViewTest
//
//  Created by wpeople on 13. 2. 19..
//  Copyright (c) 2013년 hyeyoung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
