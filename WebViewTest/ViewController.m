//
//  ViewController.m
//  WebViewTest
//
//  Created by wpeople on 13. 2. 19..
//  Copyright (c) 2013년 hyeyoung. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize webView;
@synthesize btnBack;
@synthesize btnForward;
@synthesize urlSearchBar;
@synthesize blockView;

/*
 2012.2.20 What should I do
 
 1. 검색창에 url입력 후 done버튼 눌렀을 때 해당 url로 화면 이동 시키기
    - 키보드 url용으로 설정, - 키보드 search와 검색창 옆의 cancle 버튼 이벤트 잡아야 함 
 
 2. back, forward 버튼 작동 확인 및 url입력 취소 버튼 만들기
 
 3. 화면 다듬기
    1) 스크롤뷰 올릴때 주소 검색창도 함께 화면밖으로 이동하도록 처리
    2) 주소입력창 말고 검색창 하나 더 만들어 보기 like safari browser 
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    currentUrl = [[NSMutableString alloc]init];
    
    // 초기 url 설정 
    NSURL *baseUrl = [[NSURL alloc]initWithString:@"http://www.google.com/"];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:baseUrl];
    [self.webView loadRequest:request];
    
    // delegate 연결
    self.webView.delegate = self;
    self.urlSearchBar.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/***************************************************************************
 * UIWebViewDelegate 메소드들
 
 : The UIWebViewDelegate protocol defines methods that a delegate of a UIWebView object can optionally implement to intervene when web content is loaded.
***************************************************************************/

// Sent before a web view begins loading a frame.
// 웹뷰가 프레임 로딩을 시작하기 전에 호출됨. 
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"웹뷰가 프레임 로딩을 시작하기 전에 호출됨. ");
    return YES;
}

// Sent after a web view starts loading a frame.
// 웹뷰가 프레임 로딩을 시작한 후에 호출됨. 
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"웹뷰가 프레임 로딩을 시작한 후에 호출됨. ");

}

// Sent after a web view finishes loading a frame.
// 웹뷰가 프레임 로딩을 끝낸 후에 호출됨.
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"웹뷰가 프레임 로딩을 끝낸 후에 호출됨.");

}

// Sent if a web view failed to load a frame.
// 웹뷰가 프레임 로딩에 실패하는 경우 호출됨. 
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"웹뷰가 프레임 로딩에 실패하는 경우 호출됨. ");

}

/***************************************************************************
 * UIWebView 메소드들
 ***************************************************************************/

// Sets the main page content and base URL.
// 메인페이지 내용과 기본 URL을 세팅함.
- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL
{
    NSLog(@"메인페이지 내용과 기본 URL을 세팅함.");
}

// Connects to a given URL by initiating an asynchronous client request.
// 비동기 클라이이언트 요청에 의해 초기화 된 URL로 연결함.
- (void)loadRequest:(NSURLRequest *)request
{
    NSLog(@"비동기 클라이이언트 요청에 의해 초기화 된 URL로 연결함.");
}

// Stops the loading of any web content managed by the receiver.
// 리시버에 의해 처리된 웹 컨텐츠의 로딩을 멈춤.
- (void)stopLoading
{
    NSLog(@"리시버에 의해 처리된 웹 컨텐츠의 로딩을 멈춤.");
}

/***************************************************************************
 * UISearchBarDelegate 메소드들
 ***************************************************************************/

// Tells the delegate when the user begins editing the search text.
// 유저가 검색어을 입력하기 시작하면 이를 delegate에게 알림.
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"유저가 검색어을 입력하기 시작하면 이를 delegate에게 알림.");
    [blockView setHidden:NO];   // 투명뷰 보이기 
    [urlSearchBar setShowsCancelButton:YES];    // 취소 버튼 보이기 
}

// Tells the delegate that the user changed the search text.
// 유저가 검색어를 변경했음을 delegate에게 알림.
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"유저가 검색어를 변경했음을 delegate에게 알림.");
}

// Tells the delegate that the user finished editing the search text.
// 유저가 검색어 입력을 마치면 이를 delegate에게 알림.
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"유저가 검색어 입력을 마치면 이를 delegate에게 알림.");
    [blockView setHidden:YES];  // 투명뷰 감추기
    [urlSearchBar setShowsCancelButton:NO];     // 취소 버튼 감추기 
}

// Tells the delegate that the cancel button was tapped.
// 취소 버튼이 선택되면 delegate에게 이를 알림.
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    // TODO. 현재 페이지 url값을 가지고 있다가 입력이 취소되면 searchBar text에 표시하기
    urlSearchBar.text = currentUrl;
}

// Tells the delegate that the search button was tapped.
// search 버튼이 선택되면 delegate에게 이를 알림.
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSMutableString *inputUrl = [[NSMutableString alloc]initWithString:@"http://"];
    
    [currentUrl setString:urlSearchBar.text];

    NSURL *connectionUrl = [[NSURL alloc]initWithString:[inputUrl stringByAppendingString:currentUrl]];
     
     NSLog(@"inputUrl확인: %@", [inputUrl stringByAppendingString:urlSearchBar.text]);
     
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:connectionUrl];
    [self.webView loadRequest:request];
    
    [searchBar resignFirstResponder];
}

/***************************************************************************
 * UIScrollViewDelegate 메소드들
 ***************************************************************************/

// Tells the delegate when the user scrolls the content view within the receiver.
// 리시버 내에서 유저가 컨텐트뷰를 스크롤 할 때 delegate에게 알림.
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"리시버 내에서 유저가 컨텐트뷰를 스크롤 할 때 delegate에게 알림.");
}

// Tells the delegate that the scroll view scrolled to the top of the content.
// 컨텐츠의 상단까지 스크롤뷰가 스크롤되면 delegate에게 알림. 
- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    NSLog(@"컨텐츠의 상단까지 스크롤뷰가 스크롤되면 delegate에게 알림. ");
}

@end
