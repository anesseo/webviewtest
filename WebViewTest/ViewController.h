//
//  ViewController.h
//  WebViewTest
//
//  Created by wpeople on 13. 2. 19..
//  Copyright (c) 2013년 hyeyoung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate, UIScrollViewDelegate, UISearchBarDelegate>
{
    NSMutableString *currentUrl;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnForward;
@property (weak, nonatomic) IBOutlet UISearchBar *urlSearchBar;
@property (weak, nonatomic) IBOutlet UIView *blockView;

@end
